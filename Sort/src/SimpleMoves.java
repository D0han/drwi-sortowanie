import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.LightSensor;
import lejos.nxt.Sound;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.util.Delay;


public class SimpleMoves {
	
	private static DifferentialPilot pilot;

	public SimpleMoves() {
		pilot = new DifferentialPilot(4.0f, 4.0f, Motor.A, Motor.B);
	}
	
	// Skr�ca w prawo
	public void goRight() {
		pilot.setRotateSpeed(50);
        pilot.rotate(-200.0, false);
        pilot.stop();
	}
	
	public void goBack() {
		Motor.A.setSpeed(90);
		Motor.B.setSpeed(89);
		Motor.A.backward();
		Motor.B.backward();
		Delay.msDelay(2000);
	}
	
	//Skr�ca w lewo
	public void goLeft() {
        pilot.setRotateSpeed(50);
        pilot.rotate(200.0, false);
        pilot.stop();
	}
	
	//Obraca o 180 stopni
	public void rotate180() {
		pilot.setRotateSpeed(50);
        pilot.rotate(400.0, false);
        pilot.stop();
	}
	
	//Otwiera rami� chwytaj�ce klocek
	public void openArm() {
		System.out.print("open arm");
		Motor.C.setSpeed((float)80);
		Motor.C.backward();
	 	Delay.msDelay(2000);
		Motor.C.stop();
	}
	
	//Zamyka rami� chwytaj�ce klocek
	public void closeArm() {
		System.out.print("close arm");
		Motor.C.setSpeed((float)80);
		Motor.C.forward();
		Delay.msDelay(2000);
		Motor.C.stop();
	}
}
