import lejos.nxt.Motor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.util.Delay;


public class Robot {
	
	private static DifferentialPilot pilot;
	private Moves moves = new Moves();
	private static int currentLine = -1;
	private static int rotation;
	static int[] wysokosc = new int[8];
	int[] sorted = new int[8];
	static Robot robot = new Robot();
	
	//ca�a logika dzia�ania robota
	private void measureBlocks() {

		robot.moves.followLine(0,0);
		int wys;
		wys = robot.moves.wysokosc();
		wysokosc[currentLine] = wys;
		
		Motor.A.stop();
		Motor.B.stop();
		robot.moves.simpleMoves.closeArm();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goBack();
		//robot.moves.simpleMoves.rotate180();
		//robot.moves.followLine(1, 15);
		
		robot.moves.simpleMoves.openArm();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goRight();
		currentLine = robot.moves.goToStart(currentLine);
		
	}
	
	private void measureBlocks2() {

		robot.moves.followLine(0,0);
		int wys;
		wys = robot.moves.wysokosc();
		wysokosc[currentLine] = wys;
		
		Motor.A.stop();
		Motor.B.stop();
		robot.moves.simpleMoves.closeArm();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goBack();
	//	robot.moves.simpleMoves.rotate180();
	//	robot.moves.followLine(1, 15);
		
		//robot.moves.simpleMoves.openArm();
		//robot.moves.simpleMoves.goBack();
		//robot.moves.simpleMoves.goBack();
		
	}
	
	private void sortBlocks() {
		robot.moves.followLine(0,0);
		//int wys;
		//wys = robot.moves.wysokosc();
		//wysokosc[currentLine] = wys;
		
		Motor.A.stop();
		Motor.B.stop();
		robot.moves.simpleMoves.closeArm();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.rotate180();
		robot.moves.followLine(1, 15);
		
		robot.moves.simpleMoves.openArm();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goBack();
		robot.moves.simpleMoves.goLeft();
		//currentLine = robot.moves.goToStart(currentLine);
	}
	
	private void sort() {
		for (int i=0; i<8; i++) {
			sorted[i] = wysokosc[i];
			//System.out.print(sorted[i] + ",");
		}
		int temp;
		int zmiana = 1;
		while(zmiana > 0){
		zmiana = 0;
			for(int i=0; i<sorted.length-1; i++){
				if(sorted[i]>sorted[i+1]){
					temp = sorted[i+1];
					sorted[i+1] = sorted[i];
					sorted[i] = temp;
					zmiana++;
				}
			}
		}
		System.out.print("Posortowane: ");
		for (int i=0; i<8;i++)
		System.out.print(sorted[i] + ",");
	}
	
	public void goTo(int lineNumber) { 
		int wynik = lineNumber - currentLine;
		if (wynik > 0) {
			//robot.moves.simpleMoves.goLeft();
			if (0<wynik)
			pilot.setRotateSpeed(50);
			pilot.rotate(-200);
			System.out.println("w prawo");
			robot.moves.goToNextRightLine(currentLine, lineNumber);

		} else {
			pilot.setRotateSpeed(50);
			pilot.rotate(200);
			System.out.println("w lewo");
			robot.moves.goToNextLeftLine(currentLine, lineNumber);
		}
	}
	
	public static void main(String[] args) {
		
		pilot = new DifferentialPilot(4.0f, 4.0f, Motor.A, Motor.B);
		currentLine = robot.moves.goToStart(currentLine);
		for (int i=0; i<7; i++) {
			robot.measureBlocks();
			//currentLine++;
			System.out.println(currentLine);
		}
		robot.measureBlocks2();
		
		robot.sort();
		

		
		//robot.moves.simpleMoves.rotate180();
//		for (int i=5; i<8;i++) {
//			for (int j=0; j<8; j++) {
//				if (robot.sorted[i] == wysokosc[j]) {
//					robot.goTo(j);
//				}
//			}
//		}
//		robot.moves.idz();
//		robot.moves.idz();
//		robot.moves.idz();
//		robot.moves.idz();
//		currentLine = 0;
//		robot.sortBlocks();
//		robot.goTo(3);
//		robot.moves.simpleMoves.openArm();
//		robot.moves.simpleMoves.goBack();
//		currentLine = 4;
//		robot.goTo(0);
//		currentLine = 0;
//		robot.goTo(4);
	}

}
