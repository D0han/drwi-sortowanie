import lejos.nxt.*;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.util.Delay;

public class Moves {
	private static DifferentialPilot pilot;
	public LightSensor lightSen;
	public SimpleMoves simpleMoves;
	public static UltrasonicSensor ultraSens;
	int i=0;
	
	public Moves() {
		pilot = new DifferentialPilot(4.0f, 4.0f, Motor.A, Motor.B);
		lightSen = new LightSensor(SensorPort.S3);
		simpleMoves = new SimpleMoves();
		ultraSens = new UltrasonicSensor(SensorPort.S2);
		ultraSens.continuous();
	}
	
	public void followLine(int tryb, int kroki) {
		pilot.setRotateSpeed(98);
		int lightVal = 0;
		int rotate = 10;
		int pom = 5;
		int kierunek = -1;
        lightVal = lightSen.readValue();
        while (lightVal >= 35) {
        	pilot.rotate((rotate*kierunek) + (pom*kierunek));
        	kierunek = kierunek*(-1);
        	pom=pom+10;
        	lightVal = lightSen.readValue();
        }
        //pilot.setTravelSpeed(30*kier);
       // pilot.travel(2);
        Motor.A.setSpeed(98);
        Motor.B.setSpeed(98);
        Motor.A.forward();
        Motor.B.forward();
        //System.out.println("przod");
        Delay.msDelay(300);

        if (tryb == 0)
        	if(ultraSens.getDistance()>19) {
        		followLine(0,0);
        	}
        if (tryb == 1 && kroki > 0) {
        	followLine(1,kroki -1);
        }
        }
	
	// przechodzenie do najbli�szej linii po prawej stronie
	public int goToStart(int currentLine) {
		pilot.setTravelSpeed(30);
		pilot.travel(10);
		int lightVal = 0;
		lightVal = lightSen.readValue();
		if(lightVal >= 35) {
			pilot.travel(5);
			
		}
		else {
			pilot.travel(3);
			pilot.setRotateSpeed(50);
			pilot.rotate(200);
		}
		return ++currentLine;
	}
	
	// przechodzenie do najbli�szej linii po lewej stronie
	public int goToNextLeftLine(int currentLine, int lineNumber) {
//		pilot.setTravelSpeed(30);
//		//pilot.travel(10);
//		int lightVal = 0;
//		lightVal = lightSen.readValue();
//		if(lightVal >= 35) {
//			pilot.travel(1);
//			goToNextLeftLine(currentLine, lineNumber);
//			System.out.print("goToNextLeftLine");
//		}
//		else {
//			pilot.travel(2);
//			pilot.setRotateSpeed(50);
//			pilot.rotate(200);
			if(currentLine != lineNumber) {
				idz();
				currentLine--;
				System.out.println("jestem tu w lewo ide");
				System.out.println(currentLine + ", " + lineNumber);
				//Delay.msDelay(10000);
				goToNextLeftLine(currentLine, lineNumber);
			}

			else {
				pilot.rotate(-200);
				followLine(0, 0);
				
				}
		return currentLine;
	}
	
	public void idz() {
		pilot.setTravelSpeed(30);
		//pilot.travel(10);
		int lightVal = 0;
		lightVal = lightSen.readValue();
		if(lightVal >= 35) {
			pilot.travel(1);
			idz();
		}
		else {
			pilot.travel(4);
		}
	}
	
	public int goToNextRightLine(int currentLine, int lineNumber) {
//		pilot.setTravelSpeed(30);
//		//pilot.travel(10);
//		int lightVal = 0;
//		lightVal = lightSen.readValue();
//		if(lightVal >= 35) {
//			pilot.travel(1);
//			goToNextRightLine(currentLine, lineNumber);
//			System.out.println("goToNextRight");
//		}
//		else {
//			pilot.travel(2);
//			pilot.setRotateSpeed(50);
//			pilot.rotate(200);
			if(currentLine != lineNumber) {
				idz();
				currentLine++;
				System.out.print("jestem tu w prawo ide");
				//Delay.msDelay(10000);
				goToNextRightLine(currentLine, lineNumber);
			}

			else {
				pilot.rotate(200);
				//pilot.travel(2);
				//pilot.setRotateSpeed(50);
				//pilot.rotate(200); obroc w strone klockow MA BYC
				}
		return currentLine;
}
	
	// doj�cie do ko�ca linii
	public void goToEndOfLine() {
		//TODO
	}
	
	//idz do klocka o okreslonym nr 
	public void goToBrickByNr(int brickNumber) {
		
	}
	
	public void goToBrickByHeight(double height) {

	}
	
	public int wysokosc() {
		int	w = ultraSens.getDistance();
		return w;
	}
	
	public void searchForBrick() {
		
	}
}
